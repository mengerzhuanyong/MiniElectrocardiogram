//
//  KnowledgeViewController.m
//  Riese
//
//  Created by air on 2019/11/4.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "KnowledgeViewController.h"
#import "KnowledgeCell.h"

#import "WebViewController.h"

@interface KnowledgeViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *mainTable;
@property (nonatomic, strong) NSArray *dataArr;

@end

@implementation KnowledgeViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = NSLocalizedString(@"Heart Knowledge", nil);
    self.view.backgroundColor = kColor(243, 243, 243);
    
    [SVProgressHUD show];
    [self makeUI];
    [self getData];
}

- (void)getData {
    [MMNetworkingManager GET:@"api/index/get_pdf_list" params:nil success:^(NSDictionary *object) {
        [SVProgressHUD dismiss];
        [self.mainTable.mj_header endRefreshing];
        NSInteger code = [object[@"code"] integerValue];
        if (code == 1) {
            self.dataArr = object[@"data"];
            [self.mainTable reloadData];
        }
        else {
            [YJProgressHUD showMessage:object[@"msg"] inView:self.view];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [self.mainTable.mj_header endRefreshing];
        [YJProgressHUD showMessage:@"网络错误" inView:self.view];
    }];
}

- (void)makeUI {
    self.mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kBottomSafeHeight)];
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;
    self.mainTable.backgroundColor = [UIColor clearColor];
    self.mainTable.showsVerticalScrollIndicator = NO;
    self.mainTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.mainTable];
}

#pragma mark - tableView delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"KnowledgeCell";
    KnowledgeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil].firstObject;
    }
    
    if (self.dataArr.count > 0) {
        cell.titleStr = self.dataArr[indexPath.row][@"title"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WebViewController *vc = [WebViewController new];
    vc.urlStr = self.dataArr[indexPath.row][@"pdf_path"];
    vc.titleStr = self.dataArr[indexPath.row][@"title"];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
