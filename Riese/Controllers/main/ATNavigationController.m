//
//  ATNavigationController.m
//  AppreciateTaste
//
//  Created by App开发者 on 2018/11/12.
//  Copyright © 2018 聂秀英. All rights reserved.
//

#import "ATNavigationController.h"

@interface ATNavigationController ()

@end

@implementation ATNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationBar setTitleTextAttributes:@{
                                                 NSForegroundColorAttributeName: [UIColor whiteColor],
                                                 NSFontAttributeName:[UIFont fontWithName:@"PingFangSC-Regular" size: 16]
                                                 }];
    //设置导航背景色v #5CBBC0   22A3FA   0x5CBBC0
    self.navigationBar.barTintColor = UIColorFromHex(0x5CBBC0);//[UIColor whiteColor];
    self.navigationBar.tintColor = [UIColor whiteColor];//kColor(35, 35, 35);
    self.navigationBar.shadowImage = [[UIImage alloc] init];
    
    
}
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    viewController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:nil action:nil];
    
    [super pushViewController:viewController animated:animated];
}
+ (void)initialize
{
    if (self == [ATNavigationController class]) {
        UINavigationBar *navigationBar = [UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[self]];
        // 使用自己的图片替换原来的返回图片
        navigationBar.backIndicatorImage = [UIImage imageNamed:@"arrow_back.png"];
        navigationBar.backIndicatorTransitionMaskImage = [UIImage imageNamed:@"arrow_back.png"];
    }
}
@end
