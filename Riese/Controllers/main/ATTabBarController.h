//
//  ATTabBarController.h
//  AppreciateTaste
//
//  Created by App开发者 on 2018/11/12.
//  Copyright © 2018 聂秀英. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
@interface ATTabBarController : UITabBarController
@property (nonatomic, strong) HomeViewController *homeVC;
+ (ATTabBarController *)shareTabBarViewController;
@end


