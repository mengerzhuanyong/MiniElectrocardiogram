//
//  ContactViewController.m
//  Riese
//
//  Created by air on 2019/10/26.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "ContactViewController.h"
#import "ContactCell.h"

@interface ContactViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *mainTable;
@property (nonatomic, strong) NSArray *dataArray;

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = NSLocalizedString(@"Contact Us", nil);
    self.view.backgroundColor = kColor(243, 243, 243);
    
    self.dataArray = @[NSLocalizedString(@"Leave a messsge", nil), NSLocalizedString(@"Email", nil), NSLocalizedString(@"Visit Us", nil)];
    
    [self makeUI];
}

- (void)makeUI {
    self.mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.mainTable.backgroundColor = [UIColor clearColor];
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;
    self.mainTable.showsVerticalScrollIndicator = NO;
    self.mainTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.mainTable];
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 15)];
    header.backgroundColor = kColor(243, 243, 243);
    self.mainTable.tableHeaderView = header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"ContactCell";
    ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil].firstObject;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.titleStr = self.dataArray[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 82;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        // leave a message
//        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"https://www.riesemed.com/contact-us"] options: @{} completionHandler: nil];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"https://www.riesemed.de/contact-us"] options: @{} completionHandler: nil];

    }
    else if (indexPath.row == 1) {
        //创建可变的地址字符串对象：
        NSMutableString *mailUrl = [[NSMutableString alloc] init];
        //添加收件人：
//        NSArray *toRecipients = @[@"ekg@riesemed.com"];
        NSArray *toRecipients = @[@"fragen@riesemed.de"];

        // 注意：如有多个收件人，可以使用componentsJoinedByString方法连接，连接符为@","
        [mailUrl appendFormat:@"mailto:%@", toRecipients[0]];
        //添加抄送人：
//        NSArray *ccRecipients = @[@"1780575208@qq.com"];
//        [mailUrl appendFormat:@"?cc=%@", ccRecipients[0]];
        // 添加密送人：
//        NSArray *bccRecipients = @[@"1780575208@qq.com"];
//        [mailUrl appendFormat:@"&bcc=%@", bccRecipients[0]];
        
        //添加邮件主题和邮件内容：
//        [mailUrl appendString:@"&subject=my email"];
//        [mailUrl appendString:@"&body=<b>Hello</b> World!"];
        //打开地址，这里会跳转至邮件发送界面：
//        NSString *emailPath = [mailUrl stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        NSString *emailPath = [mailUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:emailPath] options:@{} completionHandler:nil];
    }
    else {
//        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"https://www.riesemed.com"] options: @{} completionHandler: nil];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"https://www.riesemed.de"] options: @{} completionHandler: nil];

    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
