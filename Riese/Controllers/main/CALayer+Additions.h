//
//  CALayer+Additions.h
//  Power
//
//  Created by App开发者 on 2019/7/19.
//  Copyright © 2019 奚春竹. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface CALayer (Additions)

@property (nonatomic, strong) UIColor *borderUIColor;
@property (nonatomic, strong) UIColor *shadowUIColor;

@end

NS_ASSUME_NONNULL_END
