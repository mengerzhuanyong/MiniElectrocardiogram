//
//  WebViewController.h
//  jingtai
//
//  Created by App开发者 on 2019/3/22.
//  Copyright © 2019 奚春竹. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WebViewController : UIViewController

@property (nonatomic, strong) NSString *urlStr;
@property (nonatomic, strong) NSString *titleStr;

@end

NS_ASSUME_NONNULL_END
