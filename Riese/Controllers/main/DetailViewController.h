//
//  DetailViewController.h
//  Riese
//
//  Created by air on 2019/10/24.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailViewController : UIViewController

@property (nonatomic, strong) NSDictionary *dataDic;

@end

NS_ASSUME_NONNULL_END
