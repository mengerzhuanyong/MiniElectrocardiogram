//
//  Common.h
//  jingtai
//
//  Created by App开发者 on 2019/3/22.
//  Copyright © 2019 奚春竹. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Common : NSObject

+(NSData *)zipNSDataWithImage:(UIImage *)sourceImage;
+ (CGSize)calculateStringSize: (NSString *)string withFont:(UIFont *)font;
+ (CGFloat)getStringHeightWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)width;
+ (NSString *)arrayToJSONString:(NSArray *)array;
+ (NSString *)turnString:(NSString *)str;

@end

NS_ASSUME_NONNULL_END
