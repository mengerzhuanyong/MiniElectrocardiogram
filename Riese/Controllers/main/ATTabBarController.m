//
//  ATTabBarController.m
//  AppreciateTaste
//
//  Created by App开发者 on 2018/11/12.
//  Copyright © 2018 聂秀英. All rights reserved.
//

#import "ATTabBarController.h"
#import "ATNavigationController.h"
#import "HomeViewController.h"
//#import "OrderViewController.h"
//#import "KnowledgeViewController.h"
//#import "VipViewController.h"

@interface ATTabBarController ()
@end

@implementation ATTabBarController

static ATTabBarController *shareTabBarViewController = nil;
static dispatch_once_t once;
//创建单利
+ (ATTabBarController *)shareTabBarViewController{
    dispatch_once(&once, ^{
        shareTabBarViewController = [[self alloc] init];
    });
    return shareTabBarViewController;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpTabBarItem];
}
-(void)setUpTabBarItem{
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    self.homeVC=[HomeViewController new];
    ATNavigationController *homeNvc = [[ATNavigationController alloc] initWithRootViewController:self.homeVC];
    
//    OrderViewController *orderVc = [OrderViewController new];
//    ATNavigationController *orderNvc = [[ATNavigationController alloc]initWithRootViewController:orderVc];
//
//    KnowledgeViewController *knowledgeVc = [KnowledgeViewController new];
//    ATNavigationController *knowledgeNvc = [[ATNavigationController alloc]initWithRootViewController:knowledgeVc];
//
//    VipViewController *vipVc = [VipViewController new];
//    ATNavigationController *vipNvc = [[ATNavigationController alloc] initWithRootViewController:vipVc];
    
    self.viewControllers = @[homeNvc];//, orderNvc, knowledgeNvc, vipNvc];
    
    //设置字体颜色跟字号
//    NSMutableDictionary *attrsN = [NSMutableDictionary dictionary];
//    attrsN[NSFontAttributeName] = kRegularFont(12);
//    attrsN[NSForegroundColorAttributeName] = kColor(102, 102, 102);
//    [[UITabBarItem appearance] setTitleTextAttributes:attrsN forState:UIControlStateNormal];
//    //
//    NSMutableDictionary *attrsS = [NSMutableDictionary dictionary];
//    attrsS[NSFontAttributeName] = kRegularFont(12);
//    attrsS[NSForegroundColorAttributeName] = kColor(214, 47, 53);
//    [[UITabBarItem appearance] setTitleTextAttributes:attrsS forState:UIControlStateSelected];
//    //设置tabBar上显示内容
//    UITabBarItem *item1 = [[UITabBarItem alloc] initWithTitle:@"首页" image:[UIImage imageNamed:@"home_normal"] selectedImage:[[UIImage imageNamed:@"home_select"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    homeNvc.tabBarItem=item1;
    
//    UITabBarItem *item2 = [[UITabBarItem alloc] initWithTitle:@"我的订单" image:[UIImage imageNamed:@"order_normal"] selectedImage:[[UIImage imageNamed:@"order_select"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    orderNvc.tabBarItem=item2;
//
//    UITabBarItem *item3 = [[UITabBarItem alloc] initWithTitle:@"消防知识" image:[UIImage imageNamed:@"knowledge_normal"] selectedImage:[[UIImage imageNamed:@"knowledge_select"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    knowledgeNvc.tabBarItem=item3;
//
//    UITabBarItem *item4 = [[UITabBarItem alloc] initWithTitle:@"会员" image:[UIImage imageNamed:@"vip_normal"] selectedImage:[[UIImage imageNamed:@"vip_select"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    vipNvc.tabBarItem=item4;
}
@end
