//
//  ReportViewController.h
//  Riese
//
//  Created by air on 2019/10/26.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportViewController : UIViewController

@property (nonatomic, strong) NSDictionary *dataDic;
@property (nonatomic, assign) float gain;
@property (nonatomic, assign) float n;
@property (nonatomic, assign) float pixelPerPoint;
@property (nonatomic, strong) NSString *gainStr;
@property (nonatomic, strong) NSString *speedStr;
@property (nonatomic, strong) NSString *result;
@property (nonatomic, strong) NSString *isRight;

@end

NS_ASSUME_NONNULL_END
