//
//  DetailViewController.m
//  Riese
//
//  Created by air on 2019/10/24.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "DetailViewController.h"
#import "HeartLive.h"
#import "ReportViewController.h"
#import "AppDelegate.h"
#import "JHRotatoUtil.h"

#define CONTENTVIEW_TOP_ROOTVIEW_MARGIN 40

@interface DetailViewController () {
    double xCoordinateInMoniter;
    double gain;  // 增益: 5, 10, 20 对应: 0.05, 0.1, 0.2
    double n;     // 增益档位: 对应: 0.5, 1, 2
    double pixelPerPoint; // 走速: 对应 2, 4, 8
    UIButton *gainBtn;
    UIButton *speedBtn;
    NSString *result;
    double ybase;
    UIInterfaceOrientation   _lastOrientation;
}

@property (nonatomic, strong) UIScrollView *scroll;
@property (nonatomic, strong) UIView *whiteView1;
@property (nonatomic, strong) UIView *whiteView2;
@property (nonatomic, strong) UILabel *resultLabel;

@property (nonatomic , strong) NSArray *dataSource;

@property (nonatomic , strong) HeartLive *refreshMoniterView;

@property (nonatomic, assign) NSInteger number;
@property (nonatomic, assign) NSInteger row; // 折线图第几行
@property (nonatomic, assign) NSInteger lineY; // 每一行线起点

@property (nonatomic, strong) PointContainer *container;

@property (nonatomic, strong) NSString *isNext;

@end

@implementation DetailViewController

-(void)updateViewConstraints {
    [super updateViewConstraints];
//    if (IS_IPHONE6PLUS) {
//
//    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.Rotate = 1;
    self.isNext = @"0";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.container clear];
    
    if([JHRotatoUtil isOrientationLandscape]) {
        if ([self.isNext isEqualToString:@"0"]) {
            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            delegate.Rotate = 0;
            [JHRotatoUtil forceOrientation: UIInterfaceOrientationPortrait];
        }
    }
    else {
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        delegate.Rotate = 0;
    }
}

- (HeartLive *)refreshMoniterView
{
    if (!_refreshMoniterView) {
        CGFloat w = (NSInteger)((SCREEN_WIDTH - 30)/30) * 30 + 2;
        CGFloat xOffset = (SCREEN_WIDTH - 30 - w)/2;
        _refreshMoniterView = [[HeartLive alloc] initWithFrame:CGRectMake(xOffset, 10, w, 600)];
        _refreshMoniterView.backgroundColor = [UIColor whiteColor];
    }
    return _refreshMoniterView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.title = NSLocalizedString(@"Report", nil);
    
    xCoordinateInMoniter = 0;
    [self.container clear];
    self.container = [PointContainer sharedContainer];
    
    self.number = 0;
    self.row = 0;
    self.lineY = 0;
    ybase = 60;
    
    NSString *gainStr = defaults(@"gain");
    if ([gainStr isEqualToString:@"5mm/mV"]) {
        gain = 60/1000.0;
        n = 0.5;
    }
    else if ([gainStr isEqualToString:@"10mm/mV"]) {
        gain = 60/1000.0;
        n = 1;
    }
    else {
        gain = 60/1000.0;
        n = 2;
    }
    
    NSString *speedStr = defaults(@"speed");
    if ([speedStr isEqualToString:@"12.5mm/s"]) {
        pixelPerPoint = 5/5.0;
    }
    else if ([speedStr isEqualToString:@"25mm/s"]) {
        pixelPerPoint = 6/10.0;
    }
    else {
        pixelPerPoint = 6/20.0;
    }

    [self getResult];
    
    [self makeNav];
    [self makeUI];

    self.view.backgroundColor = kColor(243, 243, 243);
    
    void (^createData)(void) = ^{
        NSString *tempString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"longtextData" ofType:@"txt"] encoding:NSUTF8StringEncoding error:nil];
        NSMutableDictionary * dic = [[NSMutableDictionary alloc] initWithDictionary:[self dictionaryWithJsonString:tempString]];
        NSArray * dataStrArray = [dic[@"DeviceData"] valueForKey:@"data"];
        NSLog(@"dataStr is%@",dataStrArray);
        tempString = [tempString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        NSMutableArray *tempData = [dataStrArray[0] mutableCopy];//[[tempString componentsSeparatedByString:@","] mutableCopy];
        [tempData enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSNumber *tempDataa = @([obj integerValue]);
            [tempData replaceObjectAtIndex:idx withObject:tempDataa];
        }];
        
        NSMutableArray * dataArray = [NSMutableArray array];
        NSArray *arr = self.dataDic[@"data"];
        for (int i = 0; i < arr.count; i++) {
            [dataArray addObject:arr[i]];
        }
        
        self.dataSource = dataArray;
        [self timerRefresnFun];
    };
    createData();
}

- (void)getResult {
    // 结果
    result = @"";
    NSArray *resultArr = @[
        self.dataDic[@"sort1"],
        self.dataDic[@"sort2"],
        self.dataDic[@"sort3"],
        self.dataDic[@"sort4"],
        self.dataDic[@"sort5"],
        self.dataDic[@"sort6"],
    ];
    NSArray *resultStrArr = @[
        NSLocalizedString(@"noabnormal", nil),
        NSLocalizedString(@"missedbeat", nil),
        NSLocalizedString(@"accidentalVPB", nil),
        NSLocalizedString(@"trigeminy", nil),
        NSLocalizedString(@"VPBbigeminy", nil),
        NSLocalizedString(@"VPBcouple", nil),
        NSLocalizedString(@"VPBrunsof3", nil),
        NSLocalizedString(@"VPBrunsof4", nil),
        NSLocalizedString(@"ront", nil),
        NSLocalizedString(@"bradycardia", nil),
        NSLocalizedString(@"tachycardia", nil),
        NSLocalizedString(@"arrhythmia", nil),
        NSLocalizedString(@"STelevation", nil),
        NSLocalizedString(@"STdepression", nil)
    ];
    
    for (int i = 0; i < resultArr.count; i++) {
        NSString *str = [resultArr[i] integerValue] == 0 ? @"" : resultStrArr[[resultArr[i] integerValue]];
        if (str.length > 0) {
            result = [NSString stringWithFormat:@"%@ %@", result, str];
        }
    }
    
    if (result.length == 0) {
        result = resultStrArr[0];
    }
}

- (void)makeNav {
    UIButton *rotateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rotateBtn.frame = CGRectMake(0, 0, 40, kNavHeight);
    [rotateBtn setImage:[UIImage imageNamed:@"rotate"] forState:UIControlStateNormal];
    [rotateBtn addTarget:self action:@selector(rotateBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *downloadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    downloadBtn.frame = CGRectMake(0, 0, 40, kNavHeight);
    [downloadBtn setImage:[UIImage imageNamed:@"download"] forState:UIControlStateNormal];
    [downloadBtn addTarget:self action:@selector(downloadBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:downloadBtn], [[UIBarButtonItem alloc] initWithCustomView:rotateBtn]];
}

- (void)makeUI {
    self.scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kNavBarAndStatusBarHeight - kBottomSafeHeight)];
    self.scroll.backgroundColor = [UIColor clearColor];
    self.scroll.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.scroll];
    self.scroll.sd_layout.leftSpaceToView(self.view, 0).rightSpaceToView(self.view, 0).topSpaceToView(self.view, 0).bottomSpaceToView(self.view, 0);
    
    self.whiteView1 = [UIView new];
    self.whiteView1.backgroundColor = [UIColor whiteColor];
    self.whiteView1.layer.cornerRadius = 4;
    self.whiteView1.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.2].CGColor;
    self.whiteView1.layer.shadowOffset = CGSizeMake(0,0);
    self.whiteView1.layer.shadowOpacity = 1;
    self.whiteView1.layer.shadowRadius = 7;
    [self.scroll addSubview:self.whiteView1];
    self.whiteView1.sd_layout.leftSpaceToView(self.scroll, 15).rightSpaceToView(self.scroll, 15).topSpaceToView(self.scroll, 15).heightIs(165);
    
    self.whiteView2 = [UIView new];
    self.whiteView2.backgroundColor = [UIColor whiteColor];
    self.whiteView2.layer.cornerRadius = 4;
    self.whiteView2.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.2].CGColor;
    self.whiteView2.layer.shadowOffset = CGSizeMake(0,0);
    self.whiteView2.layer.shadowOpacity = 1;
    self.whiteView2.layer.shadowRadius = 7;
    [self.scroll addSubview:self.whiteView2];
    self.whiteView2.sd_layout.leftSpaceToView(self.scroll, 15).rightSpaceToView(self.scroll, 15).topSpaceToView(self.whiteView1, 15).heightIs(400);
    
    self.resultLabel = [[UILabel alloc] init];
    self.resultLabel.text = [NSString stringWithFormat:@"%@ ：%@", NSLocalizedString(@"Result", nil), result];
    self.resultLabel.textColor = kColor(102, 102, 102);
    self.resultLabel.font = kRegularFont(14);
    [self.whiteView2 addSubview:self.resultLabel];
    
    [self.whiteView2 addSubview:self.refreshMoniterView];
    [self makeView1];
}

- (void)makeView1 {
    UIImageView *timeImageV = [UIImageView new];
    timeImageV.image = [UIImage imageNamed:@"shijian_3"];
    [self.whiteView1 addSubview:timeImageV];
    
    UILabel *timeLabel = [UILabel new];
    //[NSString stringWithFormat:@"%@/%@/%@  %@:%@:%@", self.dataDic[@"month"], self.dataDic[@"day"], self.dataDic[@"year"], self.dataDic[@"hour"], self.dataDic[@"min"], self.dataDic[@"sec"]];
    NSString *year = self.dataDic[@"year"];
    NSString *month = [Common turnString:self.dataDic[@"month"]];
    NSString *day = [Common turnString:self.dataDic[@"day"]];
    NSString *hour = [Common turnString:self.dataDic[@"hour"]];
    NSString *min = [Common turnString:self.dataDic[@"min"]];
    NSString *sec = [Common turnString:self.dataDic[@"sec"]];
    
    timeLabel.text = [NSString stringWithFormat:@"%@ : %@/%@/%@  %@:%@:%@", NSLocalizedString(@"Time", nil), month, day, year, hour, min, sec];
    timeLabel.textColor = kColor(102, 102, 102);
    timeLabel.font = kRegularFont(14);
    [self.whiteView1 addSubview:timeLabel];
    
    UIImageView *hrImageV = [UIImageView new];
    hrImageV.image = [UIImage imageNamed:@"home_c_zan"];
    [self.whiteView1 addSubview:hrImageV];
    
    UILabel *hrLabel = [UILabel new];
    hrLabel.text = [NSString stringWithFormat:@"%@ : %@bpm", NSLocalizedString(@"HR", nil),  self.dataDic[@"HeartRate"]];
    hrLabel.textColor = kColor(102, 102, 102);
    hrLabel.font = kRegularFont(14);
    [self.whiteView1 addSubview:hrLabel];
    
    UIImageView *durationImgV = [UIImageView new];
    durationImgV.contentMode = UIViewContentModeScaleAspectFit;
    durationImgV.image = [UIImage imageNamed:@"home_c_dur"];
    [self.whiteView1 addSubview:durationImgV];
    
    NSInteger duration = [self.dataDic[@"TimeLen"] integerValue]/250;
    UILabel *durationLabel = [UILabel new];
    durationLabel.text = [NSString stringWithFormat:@"%@ : %lds", NSLocalizedString(@"Duration", nil), duration];
    durationLabel.textColor = kColor(102, 102, 102);
    durationLabel.font = kRegularFont(14);
    [self.whiteView1 addSubview:durationLabel];
    
    UIImageView *resultImgV = [UIImageView new];
    resultImgV.image = [UIImage imageNamed:@"home_c_rem"];
    [self.whiteView1 addSubview:resultImgV];
    
    UILabel *resultLabel = [UILabel new];
    resultLabel.text = [NSString stringWithFormat:@"%@ : %@", NSLocalizedString(@"Result", nil), result];
    resultLabel.textColor = kColor(102, 102, 102);
    resultLabel.font = kRegularFont(14);
    [self.whiteView1 addSubview:resultLabel];
    
    UILabel *gainLabel = [UILabel new];
    gainLabel.text = [NSString stringWithFormat:@"%@ : ", NSLocalizedString(@"Gain", nil)];
    gainLabel.textColor = kColor(102, 102, 102);
    gainLabel.font = kRegularFont(14);
    [self.whiteView1 addSubview:gainLabel];
    
    gainBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    gainBtn.layer.borderWidth = 1;
    gainBtn.layer.borderColor = kColor(151, 151, 151).CGColor;
    gainBtn.layer.cornerRadius = 5;
    [gainBtn setTitle:defaults(@"gain") forState:UIControlStateNormal];
    [gainBtn setTitleColor:kColor(102, 102, 102) forState:UIControlStateNormal];
    gainBtn.titleLabel.font = kRegularFont(14);
    [gainBtn addTarget:self action:@selector(gainBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.whiteView1 addSubview:gainBtn];
    
    UILabel *speedLabel = [UILabel new];
    speedLabel.text = [NSString stringWithFormat:@"%@ : ", NSLocalizedString(@"Speed", nil)];
    speedLabel.textColor = kColor(102, 102, 102);
    speedLabel.font = kRegularFont(14);
    [self.whiteView1 addSubview:speedLabel];
    
    speedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    speedBtn.layer.borderWidth = 1;
    speedBtn.layer.borderColor = kColor(151, 151, 151).CGColor;
    speedBtn.layer.cornerRadius = 5;
    [speedBtn setTitle:defaults(@"speed") forState:UIControlStateNormal];
    [speedBtn addTarget:self action:@selector(speedBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [speedBtn setTitleColor:kColor(102, 102, 102) forState:UIControlStateNormal];
    speedBtn.titleLabel.font = kRegularFont(14);
    [self.whiteView1 addSubview:speedBtn];
    
    // layout
    timeImageV.sd_layout.leftSpaceToView(self.whiteView1, 12).topSpaceToView(self.whiteView1, 18).widthIs(15).heightIs(15);
    timeLabel.sd_layout.leftSpaceToView(timeImageV, 10).topSpaceToView(self.whiteView1, 15).rightSpaceToView(self.whiteView1, 15).heightIs(20);
    
    hrImageV.sd_layout.leftSpaceToView(self.whiteView1, 12).topSpaceToView(timeImageV, 21).widthIs(15).heightIs(15);
    hrLabel.sd_layout.leftSpaceToView(hrImageV, 10).topSpaceToView(timeLabel, 15).heightIs(20);
    [hrLabel setSingleLineAutoResizeWithMaxWidth:0];
    
    durationImgV.sd_layout.leftSpaceToView(self.whiteView1, (SCREEN_WIDTH - 30)/2).topSpaceToView(timeImageV, 21).widthIs(17).heightIs(15);
    durationLabel.sd_layout.leftSpaceToView(durationImgV, 10).topSpaceToView(timeLabel, 15).heightIs(20).rightSpaceToView(self.whiteView1, 15);
    
    resultImgV.sd_layout.leftSpaceToView(self.whiteView1, 12).topSpaceToView(hrImageV, 21).widthIs(15).heightIs(15);
    resultLabel.sd_layout.leftSpaceToView(resultImgV, 10).topSpaceToView(hrLabel, 15).rightSpaceToView(self.whiteView1, 15).heightIs(20);
    
    gainLabel.sd_layout.leftSpaceToView(self.whiteView1, 15).topSpaceToView(resultLabel, 15).heightIs(22);
    [gainLabel setSingleLineAutoResizeWithMaxWidth:0];
    gainBtn.sd_layout.leftSpaceToView(gainLabel, 0).topSpaceToView(resultLabel, 15).widthIs(80).heightIs(22);
    
    speedLabel.sd_layout.leftSpaceToView(gainBtn, 15).topSpaceToView(resultLabel, 15).heightIs(22);
    [speedLabel setSingleLineAutoResizeWithMaxWidth:0];
    speedBtn.sd_layout.leftSpaceToView(speedLabel, 0).topSpaceToView(resultLabel, 15).widthIs(80).heightIs(22);
}

- (void)gainBtnClick:(UIButton *)sender {
    if ([sender.currentTitle isEqualToString:@"10mm/mV"]) {
        [sender setTitle:@"20mm/mV" forState:UIControlStateNormal];
        gain = 60/1000.0;
        n = 2;
    }
    else if ([sender.currentTitle isEqualToString:@"20mm/mV"]) {
        [sender setTitle:@"5mm/mV" forState:UIControlStateNormal];
        gain = 60/1000.0;
        n = 0.5;
    }
    else {
        [sender setTitle:@"10mm/mV" forState:UIControlStateNormal];
        gain = 60/1000.0;
        n = 1;
    }
    
    self.number = 0;
    self.row = 0;
    self.lineY = 0;
    xCoordinateInMoniter = 0;
    ybase = 60;
    
    [self.container clear];
    self.container = [PointContainer sharedContainer];
    [self timerRefresnFun];
}

- (void)speedBtnClick:(UIButton *)sender {
    if ([sender.currentTitle isEqualToString:@"25mm/s"]) {
        [sender setTitle:@"50mm/s" forState:UIControlStateNormal];
        pixelPerPoint = 6/20.0;
    }
    else if ([sender.currentTitle isEqualToString:@"50mm/s"]) {
        [sender setTitle:@"12.5mm/s" forState:UIControlStateNormal];
        pixelPerPoint = 6/5.0;
    }
    else {
        [sender setTitle:@"25mm/s" forState:UIControlStateNormal];
        pixelPerPoint = 6/10.0;
    }
    
    self.number = 0;
    self.row = 0;
    self.lineY = 0;
    xCoordinateInMoniter = 0;
    ybase = 60;
    
    [self.container clear];
    self.container = [PointContainer sharedContainer];
    [self timerRefresnFun];
}

- (void)downloadBtnClick {
    self.isNext = @"1";
    
    [self.container clear];
    ReportViewController *vc = [ReportViewController new];
    vc.dataDic = self.dataDic;
    vc.gain = gain;
    vc.n = n;
    vc.pixelPerPoint = pixelPerPoint;
    vc.gainStr = gainBtn.currentTitle;
    vc.speedStr = speedBtn.currentTitle;
    vc.result = result;
    vc.isRight = [JHRotatoUtil isOrientationLandscape] ? @"1" : @"0";
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)rotateBtnClick {
    if([JHRotatoUtil isOrientationLandscape]) {
        
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        delegate.Rotate = 0;
        [JHRotatoUtil forceOrientation: UIInterfaceOrientationPortrait];
    }
    else {
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        delegate.Rotate = 1;
        [JHRotatoUtil forceOrientation: UIInterfaceOrientationLandscapeRight];
    }
}

//iOS8旋转动作的具体执行
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator: coordinator];
    // 监察者将执行： 1.旋转前的动作  2.旋转后的动作（completion）
    [coordinator animateAlongsideTransition: ^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         if ([JHRotatoUtil isOrientationLandscape]) {
             _lastOrientation = [UIApplication sharedApplication].statusBarOrientation;
             [self p_prepareFullScreen];
         }
         else {
             [self p_prepareSmallScreen];
         }
     } completion: ^(id<UIViewControllerTransitionCoordinatorContext> context) {
     }];
    
}

//iOS7旋转动作的具体执行
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if (toInterfaceOrientation == UIDeviceOrientationLandscapeRight || toInterfaceOrientation == UIDeviceOrientationLandscapeLeft) {
        _lastOrientation = [UIApplication sharedApplication].statusBarOrientation;
        [self p_prepareFullScreen];
    }
    else {
        [self p_prepareSmallScreen];
    }
}

#pragma mark - Private

// 切换成全屏的准备工作
- (void)p_prepareFullScreen {
    self.number = 0;
    self.row = 0;
    self.lineY = 0;
    xCoordinateInMoniter = 0;
    ybase = 60;
    
    [self.container clear];
    self.container = [PointContainer sharedContainer];
    [self timerRefresnFun];
}

// 切换成小屏的准备工作
- (void)p_prepareSmallScreen {
    self.number = 0;
    self.row = 0;
    self.lineY = 0;
    xCoordinateInMoniter = 0;
    ybase = 60;
    
    [self.container clear];
    self.container = [PointContainer sharedContainer];
    [self timerRefresnFun];
}

-(NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString{
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    if(err) {
    NSLog(@"json解析失败：%@",err);
    return nil;
    }
    return dic;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 哟
//刷新方式绘制
- (void)timerRefresnFun {
    for (int i = 0; i < self.dataSource.count; i++) {
        [self.container addPointAsRefreshChangeform:[self bubbleRefreshPoint]];
    }
    
    CGFloat w = (NSInteger)(([UIScreen mainScreen].bounds.size.width - 30)/30) * 30 + 2;
    CGFloat xOffset = ([UIScreen mainScreen].bounds.size.width - 30 - w)/2;
    self.refreshMoniterView.frame = CGRectMake(xOffset, 10, w, 60 * (self.row + 2) + 2);
    self.resultLabel.sd_layout.leftSpaceToView(self.whiteView2, 15).rightSpaceToView(self.whiteView2, 15).topSpaceToView(self.refreshMoniterView, 10).heightIs(20);
    self.whiteView2.sd_layout.leftSpaceToView(self.scroll, 15).rightSpaceToView(self.scroll, 15).topSpaceToView(self.whiteView1, 15).heightIs(self.refreshMoniterView.height + 60);
    self.scroll.contentSize = CGSizeMake(0, 210 + self.refreshMoniterView.height + 60);
    
    [self.refreshMoniterView fireDrawingWithPoints:self.container.refreshPointContainer pointsCount:self.container.numberOfRefreshElements];
}

#pragma mark - DataSource
- (CGPoint)bubbleRefreshPoint
{
    static NSInteger dataSourceCounterIndex = -1;
    dataSourceCounterIndex ++;
    dataSourceCounterIndex %= [self.dataSource count];
    
    CGFloat y = ybase - ([self.dataSource[dataSourceCounterIndex] doubleValue] - 16384) * gain * n;
    CGPoint targetPointToAdd = (CGPoint){xCoordinateInMoniter,y};
    xCoordinateInMoniter += pixelPerPoint;
    
    CGFloat w = (NSInteger)(([UIScreen mainScreen].bounds.size.width - 30)/30) * 30 + 2;
    
    if (xCoordinateInMoniter >= w) {
        xCoordinateInMoniter = 0;
        ybase += 60;
        self.row++;
    }

    return targetPointToAdd;
}

@end
