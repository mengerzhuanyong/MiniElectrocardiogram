//
//  NSBundle+Language.h
//  Riese
//
//  Created by air on 2019/11/5.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <Foundation/Foundation.h>

#define GCLocalizedString(KEY) [[NSBundle mainBundle] localizedStringForKey:KEY value:nil table:@"Localizable"]

NS_ASSUME_NONNULL_BEGIN

@interface NSBundle (Language)

+ (void)setLanguage:(NSString *)language;

@end

NS_ASSUME_NONNULL_END
