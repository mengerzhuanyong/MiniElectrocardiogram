//
//  SaveSettingViewController.h
//  Riese
//
//  Created by air on 2019/11/2.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SaveSettingViewController : UIViewController

@property (nonatomic, strong) NSString *languageStr;
@property (nonatomic, strong) NSString *lengthStr;
@property (nonatomic, strong) NSString *soundStr;
@property (nonatomic, strong) NSString *isClear;

@end

NS_ASSUME_NONNULL_END
