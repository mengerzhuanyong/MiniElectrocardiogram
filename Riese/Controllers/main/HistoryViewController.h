//
//  HistoryViewController.h
//  Riese
//
//  Created by air on 2019/10/26.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HistoryViewController : UIViewController

@property (nonatomic, strong) UIImage *avatarImg;
@property (nonatomic, strong) NSString *nickname;

@end

NS_ASSUME_NONNULL_END
