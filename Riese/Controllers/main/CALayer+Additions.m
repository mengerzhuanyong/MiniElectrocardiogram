//
//  CALayer+Additions.m
//  Power
//
//  Created by App开发者 on 2019/7/19.
//  Copyright © 2019 奚春竹. All rights reserved.
//

#import "CALayer+Additions.h"

// xib 设置 borderColor, shadowColor
@implementation CALayer (Additions)

- (void)setBorderUIColor:(UIColor *)color {
    self.borderColor = color.CGColor;
}

- (UIColor *)borderUIColor {
    return [UIColor colorWithCGColor:self.borderColor];
}

- (void)setShadowUIColor:(UIColor *)color {
    self.shadowColor = color.CGColor;
}

- (UIColor *)shadowUIColor {
    return [UIColor colorWithCGColor:self.shadowColor];
}

@end
