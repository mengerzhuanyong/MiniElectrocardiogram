//
//  ReTextField.m
//  JGW
//
//  Created by App开发者 on 2019/8/15.
//  Copyright © 2019 奚春竹. All rights reserved.
//

#import "ReTextField.h"

/* 设置 textfield leftView 位置 */
@implementation ReTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGRect)leftViewRectForBounds:(CGRect)bounds {
    CGRect textRect = [super leftViewRectForBounds:bounds];
    textRect.origin.x += 11;
    return textRect;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect textRect = [super textRectForBounds:bounds];
    textRect.origin.x += 11;
    return textRect;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    CGRect textRect = [super editingRectForBounds:bounds];
    textRect.origin.x += 11;
    return textRect;
}

- (CGRect)rightViewRectForBounds:(CGRect)bounds {
    CGRect textRect = [super rightViewRectForBounds:bounds];
    textRect.origin.x -= 15;
    return textRect;
}

@end
