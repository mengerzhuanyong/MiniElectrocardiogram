//
//  JYJPersonViewController.m
//  导航测试demo
//
//  Created by JYJ on 2017/6/5.
//  Copyright © 2017年 baobeikeji. All rights reserved.
//

#import "JYJPersonViewController.h"
//#import "JYJMyWalletViewController.h"
//#import "JYJMyCardViewController.h"
//#import "JYJMyTripViewController.h"
//#import "JYJMyFriendViewController.h"
//#import "JYJMyStickerViewController.h"
#import "JYJCommenItem.h"
#import "JYJProfileCell.h"
#import "JYJAnimateViewController.h"

@interface JYJPersonViewController () <UITableViewDelegate, UITableViewDataSource>
/** tableView */
@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) UIView * footView;
@property (nonatomic, strong) UILabel * lab_version;
@property (nonatomic, strong) FMDatabase *db;
///** headerIcon */
//@property (nonatomic, weak) UIImageView *headerIcon;
/** data */
@property (nonatomic, strong) NSArray *data;
@end

@implementation JYJPersonViewController

- (NSArray *)data {
    if (!_data) {
        self.data = [NSArray array];
    }
    return _data;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupUI];
    
    [self setupData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.frame = CGRectMake(0, 0, self.view.width, self.view.height - 30);//self.view.bounds;
    self.footView.sd_layout.leftSpaceToView(self.view, 0).bottomSpaceToView(self.view, 0).widthIs(self.view.width).heightIs(30);
    self.lab_version.sd_layout.leftEqualToView(self.footView).centerYEqualToView(self.footView).widthIs(self.footView.width).heightIs(30);
}

- (void)setupUI {
    UITableView *tableView = [[UITableView alloc] init];
    tableView.scrollEnabled = NO;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.backgroundColor = UIColorFromHex(0x5CBBC0);//[UIColor whiteColor];
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = UIColorFromHex(0x5CBBC0);//[UIColor colorWithRed:245 / 255.0 green:247 / 255.0 blue:250 / 255.0 alpha:1.0];
    headerView.frame = CGRectMake(0, 0, self.tableView.width, 150);
    self.tableView.tableHeaderView = headerView;
    
    /** 头像图片 */
    UIImageView *headerIcon = [[UIImageView alloc] init];
    headerIcon.image = [UIImage imageNamed:@"person_home_icon"];
    headerIcon.frame = CGRectMake(30, 60, 178, 65);
    [headerView addSubview:headerIcon];
    
    self.footView = [[UIView alloc] init];
    self.footView.backgroundColor = [UIColor whiteColor];
//    footView.frame  = CGRectMake(0, 0, self.tableView.width, 40);
//    self.tableView.tableFooterView = footView;
    [self.view addSubview:self.footView];
    self.footView.sd_layout.leftSpaceToView(self.view, 0).bottomSpaceToView(self.view, 0).widthIs(self.view.width).heightIs(30);
    
    self.lab_version = [[UILabel alloc] init];
    [self.footView addSubview:self.lab_version];
    self.lab_version.sd_layout.leftEqualToView(self.footView).centerYEqualToView(self.footView).widthIs(self.footView.width).heightIs(30);
    self.lab_version.backgroundColor = UIColorFromHex(0x5CBBC0);
    self.lab_version.text = kCombileABStr(@"Version :", BundleVersion);
    self.lab_version.textColor = UIColorFromHex(0x005894);
    self.lab_version.textAlignment = NSTextAlignmentCenter;
    self.lab_version.font = [UIFont systemFontOfSize:14];
}

//第一次进来调用
- (void)setupData {
    JYJCommenItem * myHome = [JYJCommenItem itemWithSelectedIcon:@"home_selected" UnSelectedIcon:@"home_unSelected" title:@"Home" subtitle:@"" destVcClass:[UIViewController class] isSelect:YES];//itemWithIcon:@"home_selected" title:@"Home" subtitle:@"" destVcClass:[UIViewController class] isSelect:NO];

    JYJCommenItem * myProfile = [JYJCommenItem itemWithSelectedIcon:@"per_pro_seleted" UnSelectedIcon:@"per_pro_unSeleted" title:@"Your Profile" subtitle:@"" destVcClass:[UIViewController class] isSelect:NO];

    JYJCommenItem * mySetting = [JYJCommenItem itemWithSelectedIcon:@"per_set_selected" UnSelectedIcon:@"per_set_unSelected" title:@"Settings" subtitle:nil destVcClass:[UIViewController class] isSelect:NO];

    JYJCommenItem * myHeart = [JYJCommenItem itemWithSelectedIcon:@"per_heart_sel" UnSelectedIcon:@"per_heart_unSel" title:@"Heart Knowledge" subtitle:nil destVcClass:[UIViewController class] isSelect:NO];
    
    JYJCommenItem * myContact = [JYJCommenItem itemWithSelectedIcon:@"per_contact_sel" UnSelectedIcon:@"per_contact_unSel" title:@"Contact Us" subtitle:nil destVcClass:[UIViewController class] isSelect:NO];
    self.data = @[myHome, myProfile, mySetting, myHeart, myContact];
}

#pragma mark - TableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // 创建cell
    
    JYJProfileCell *cell = [JYJProfileCell cellWithTableView:tableView];
    cell.item = self.data[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 51;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    for (JYJCommenItem *item in self.data) {
        item.isSelected = NO;
    }
    JYJCommenItem *item = self.data[indexPath.row];
    item.isSelected = !item.isSelected;
    [tableView reloadData];
    if (item.destVcClass == nil) return;
    
    if (indexPath.row == 1) {
        // 创建数据库对象
        NSString * docsdir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString * path = [docsdir stringByAppendingPathComponent:@"UserInfo.sqlite"];//将需要创建的串拼接到后面
        self.db = [FMDatabase databaseWithPath:path];
        // 建表
        [self createTable];
    }
//
//    JYJPushBaseViewController *vc = [[item.destVcClass alloc] init];
//    vc.title = item.title;
//    vc.animateViewController = (JYJAnimateViewController *)self.parentViewController;
//    [self.parentViewController.navigationController pushViewController:vc animated:YES];
}

#pragma mark - FMDB
// 创建表
- (void)createTable{

   if ([self.db open]) {
       //AUTOINCREMENT 自增
       if (![self isExistTable:@"userInfo"]) {
           BOOL result = [self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS UserInfo(ID INTEGER PRIMARY KEY AUTOINCREMENT,'nickname' 'text', 'sex' 'text','age' 'text');"];
           if (result) {
               //[ProgressHUD showSuccess:@"创建表成功"];
               [self addUserInfo];
               
           }else{
               [YJProgressHUD showMessage:@"创建表失败" inView:self.view];
           }
           [self.db close];
       }
       else {
           [self addUserInfo];
           [self.db close];
       }
   }else{
       [YJProgressHUD showMessage:@"数据库打开失败" inView:self.view];
   }
}

// 插入数据
- (void)addUserInfo {
    //1.准备sqlite语句
    NSString *sqlite = [NSString stringWithFormat:@"insert into userInfo(nickname,sex,age) values ('%@','%@','%@')", @"金鱼222", @"男", @"2018"];
    //2.执行sqlite语句
    //    char *error = NULL;//执行sqlite语句失败的时候,会把失败的原因存储到里面
    BOOL result = [self.db executeUpdate:sqlite];
    if (result) {
        NSLog(@"添加数据成功");
        FMResultSet *rs = [self.db executeQuery:@"select * from userInfo"];
        NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
        // 遍历结果集
        while ([rs next]) {
            NSDictionary *dic = @{@"nickname": [rs stringForColumn:@"nickname"], @"sex": [rs stringForColumn:@"sex"], @"age": [rs stringForColumn:@"age"]};
            [arr addObject:dic];
            NSLog(@"");
        }
    } else {
        NSLog(@"添加数据失败");
    }
}

/**
 判断一张表是否已经存在
 @param tablename 表名
 */
- (BOOL)isExistTable:(NSString *)tablename{
    NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM sqlite_master where type= 'table' and name= '%@';",tablename];
    FMResultSet *rs = [self.db executeQuery:sql];
//    BOOL result = [db executeQuery:sql];
    while ([rs next]) {
        NSInteger count = [rs intForColumnIndex:0];
        if (count == 0) {
            return NO;
        }else {
            return YES;
        }
    }
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
