//
//  JYJCommenItem.m
//  JYJSlideMenuController
//
//  Created by JYJ on 2017/6/16.
//  Copyright © 2017年 baobeikeji. All rights reserved.
//

#import "JYJCommenItem.h"

@implementation JYJCommenItem

+ (instancetype)itemWithSelectedIcon:(NSString *)icon UnSelectedIcon:(NSString *)unS_icon title:(NSString *)title subtitle:(NSString *)subtitle destVcClass:(Class)destVcClass isSelect:(BOOL)isSelected {
    JYJCommenItem *item = [[self alloc] init];
    item.icon_selected = icon;
    item.icon_unSelected = unS_icon;
    item.title = title;
    item.subtitle = subtitle;
    item.destVcClass = destVcClass;
    item.isSelected = isSelected;
    return item;
}

@end
