//
//  JYJProfileCell.m
//  JYJSlideMenuController
//
//  Created by JYJ on 2017/6/16.
//  Copyright © 2017年 baobeikeji. All rights reserved.
//

#import "JYJProfileCell.h"
#import "JYJCommenItem.h"

@interface JYJProfileCell ()
@property (nonatomic, strong)UIView * bgV;
/** 1.图片 */
@property (nonatomic, strong) UIImageView *iconView;
/** 2.标题 */
@property (nonatomic, strong) UILabel *nameLabel;
/** 3.subTitle */
@property (nonatomic, strong) UILabel *subtitle;
/** 4.箭头 */
@property (nonatomic, strong) UIImageView *arrowView;
/*cell底部的线*/
@property (nonatomic, strong)UIView * view_line;
@end

@implementation JYJProfileCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    // 创建cell
    static NSString *ID = @"JYJProfileCell";
    JYJProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    
    if (cell == nil) {
        cell = [[JYJProfileCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:ID];
        [cell.contentView setBackgroundColor:UIColorFromHex(0x5CBBC0)];
    }
    return cell;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // 初始化操作
    [self setupSubviews];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        // 初始化操作
        [self setupSubviews];
    }
    return self;
}

/**
 *  初始化子控件
 */
- (void)setupSubviews {
    self.bgV = [[UIView alloc] init];
    [self.contentView addSubview:self.bgV];
    
    /** 1.图片 */
    self.iconView = [[UIImageView alloc] init];
    [self.bgV addSubview:self.iconView];
    
    
    // 2.标题
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.textColor = [UIColor whiteColor];
    self.nameLabel.font = [UIFont systemFontOfSize:16];
//    self.nameLabel.backgroundColor = [UIColor greenColor];
    [self.bgV addSubview:self.nameLabel];
    
    
    /** 3.描述 */
    self.subtitle = [[UILabel alloc] init];
    self.subtitle.textColor = [UIColor colorWithRed:151 / 255.0 green:151 / 255.0 blue:151 / 255.0 alpha:1.0];
    self.subtitle.font = [UIFont systemFontOfSize:12];
    self.subtitle.textAlignment = NSTextAlignmentRight;
    [self.bgV addSubview:self.subtitle];
    
    /** 4.箭头 */
    self.arrowView = [[UIImageView alloc] init];
    self.arrowView.image = [UIImage imageNamed:@"caret_right"];
    [self.bgV addSubview:self.arrowView];
    //    self.arrowView = arrowView;
    
    self.view_line = [[UIView alloc] init];
    self.view_line.backgroundColor = UIColorFromHex(0x4FA1A5);
    [self.contentView addSubview:self.view_line];
    //    self.view_line = lineV;
}

/**
 * 设置子控件的frame
 */
- (void)layoutSubviews {
    [super layoutSubviews];
//    CGFloat contentW = self.contentView.bounds.size.width;
//    CGFloat contentH = self.contentView.bounds.size.height;
    //背景
    self.bgV.sd_layout.leftSpaceToView(self.contentView, 20).topSpaceToView(self.contentView, 5).rightSpaceToView(self.contentView, 20).widthIs(self.contentView.width - 20 - 20).heightIs(self.contentView.height - 10);
    self.bgV.layer.cornerRadius = self.bgV.frame.size.height / 2;
    self.bgV.clipsToBounds = YES;
    self.bgV.backgroundColor = self.item.isSelected ? [UIColor whiteColor] : UIColorFromHex(0x5CBBC0);
    /** 1.图片 */
    self.iconView.sd_layout.leftSpaceToView(self.bgV,11).centerYEqualToView(self.bgV).widthIs(15).heightIs(15);
    self.iconView.contentMode = UIViewContentModeScaleAspectFit;
    self.iconView.image =self.item.isSelected ? [UIImage imageNamed:self.item.icon_selected] : [UIImage imageNamed:self.item.icon_unSelected];
    
    /** 2.标题 */
    self.nameLabel.sd_layout.leftSpaceToView(self.iconView, 10).topSpaceToView(self.bgV, 10).widthIs(self.bgV.width - self.iconView.width - 15).heightIs(self.bgV.height - 10 * 2);
    self.nameLabel.text = self.item.title;
    self.nameLabel.textColor = self.item.isSelected ? UIColorFromHex(0x5CBBC0) : [UIColor whiteColor];
    /** 4.箭头 */
//    CGFloat arrowViewW = 14;
//    CGFloat arrowViewX = contentW - 15 - arrowViewW;
//    CGFloat arrowViewH = 14;
//    CGFloat arrowViewY = (contentH - arrowViewH) / 2;
//    self.arrowView.frame = CGRectMake(arrowViewX, arrowViewY, arrowViewW, arrowViewH);
    
    /** 3.子标题 */
//    CGFloat subtitleW = 150;
//    CGFloat subtitleX = CGRectGetMinX(self.arrowView.frame) - 10 - subtitleW;
//    CGFloat subtitleY = 0;
//    CGFloat subtitleH = contentH;
//    self.subtitle.frame = CGRectMake(subtitleX, subtitleY, subtitleW, subtitleH);
    
    self.view_line.sd_layout.leftSpaceToView(self.contentView, 20).bottomSpaceToView(self.contentView, 0).widthIs(self.contentView.width - 20).heightIs(1);
}

//- (void)setItem:(JYJCommenItem *)item {
//    _item = item;
//    self.iconView.image = [UIImage imageNamed:item.icon];
//    self.nameLabel.text = item.title;
//    self.subtitle.text = item.subtitle;
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
