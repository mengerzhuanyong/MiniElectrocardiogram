//
//  AddUserViewController.h
//  Riese
//
//  Created by air on 2019/10/20.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddUserViewController : UIViewController

@property (nonatomic, strong) NSString *isEdit;
@property (nonatomic, strong) NSDictionary *dataDic;

@end

NS_ASSUME_NONNULL_END
