//
//  PMAccount.m
//  PropertyManagement
//
//  Created by 杨立鹏 on 2017/6/15.
//  Copyright © 2017年 奚春竹. All rights reserved.
//

#import "PMAccount.h"

#define kAccountPath kDocumentFile(@"Account")
@implementation PMAccount

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"userId" : @"id"
             };
}

- (void)encodeWithCoder:(NSCoder *)aCoder { [self yy_modelEncodeWithCoder:aCoder]; }
- (id)initWithCoder:(NSCoder *)aDecoder { return [self yy_modelInitWithCoder:aDecoder]; }
- (id)copyWithZone:(NSZone *)zone { return [self yy_modelCopy]; }
- (NSUInteger)hash { return [self yy_modelHash]; }
- (BOOL)isEqual:(id)object { return [self yy_modelIsEqual:object]; }


@end



/**
 *  用户工具类
 */
@implementation PMAccountTool

static PMAccountTool *_tool = nil;
static dispatch_once_t once;

+ (PMAccountTool *)sharedAccount
{
    dispatch_once(&once, ^{
        _tool = [[self alloc] init];
    });
    return _tool;
}


+ (void)saveAccount:(PMAccount *)account{
    
    NSLog(@"%@",account);
    [NSKeyedArchiver archiveRootObject:account toFile:kAccountPath];
    
}

+ (PMAccount *)account{
    
    //加载模型
    PMAccount *account = [NSKeyedUnarchiver unarchiveObjectWithFile:kAccountPath];
    
    
    return account;
}
@end

