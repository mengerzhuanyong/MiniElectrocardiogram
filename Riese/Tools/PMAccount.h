//
//  PMAccount.h
//  PropertyManagement
//
//  Created by 杨立鹏 on 2017/6/15.
//  Copyright © 2017年 奚春竹. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMAccount : NSObject

@property (nonatomic, assign) NSUInteger userId;

@property (nonatomic, copy) NSString *loginName;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, copy) NSString *userTelephone;

@property (nonatomic, copy) NSString *jobNum;

@property (nonatomic, copy) NSString *token;

@property (nonatomic, strong) NSArray * sysMenuList;//权限列表

@end



/**
 *  用户工具类
 */
@interface PMAccountTool : NSObject

/**
 *  保存账号信息
 *
 *  @param account 账号模型
 */
+ (void)saveAccount:(PMAccount *)account;
/**
 *  返回账号信息
 *
 *  @return 账号模型（如果过期 会返回nil）
 */
+ (PMAccount *)account;

@end
