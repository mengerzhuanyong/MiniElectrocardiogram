//
//  AppDelegate.h
//  Riese
//
//  Created by air on 2019/10/21.
//  Copyright © 2019 wangjinyu. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) NSInteger Rotate;

@end

