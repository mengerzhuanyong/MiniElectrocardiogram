//
//  AppDelegate.m
//  Riese
//
//  Created by air on 2019/10/21.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "ATNavigationController.h"
#import <MMAlertView.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.Rotate = 0;
    
    // 当前app的信息
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];

    // app版本
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *currentVersion = [NSString stringWithFormat:@"V%@", app_Version];
    
    [MMNetworkingManager GET:@"api/index/index?version=1.0" params:nil success:^(NSDictionary *object) {
        if (object.count > 0) {
            if (![currentVersion isEqualToString:object[@"new_version"]]) {
                // 需要更新
                NSArray *items = @[MMItemMake(NSLocalizedString(@"cancel", nil), MMItemTypeNormal, nil), MMItemMake(NSLocalizedString(@"sure", nil), MMItemTypeNormal, ^(NSInteger index) {
                    // 跳到 app store 更新 app
                    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:object[@"app_store_url"]] options: @{} completionHandler: nil];
                })];
                [[[MMAlertView alloc] initWithTitle:@"" detail:@"\n发现新版本, 是否更新?" items:items] show];
            }
        }
    } failure:^(NSError *error) {
        
    }];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
    HomeViewController * homeVc = [[HomeViewController alloc] init];
    ATNavigationController *navVC = [[ATNavigationController alloc] initWithRootViewController:homeVc];
    self.window.rootViewController = navVC;
    
    [self.window makeKeyAndVisible];
    return YES;
}


#pragma mark - 横竖屏
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    // 为 1 的话说明某个页面需要横竖屏转化
    if (self.Rotate == 1) {
        return UIInterfaceOrientationMaskLandscapeRight;
    }
    else {
        // 为 0 的话说明只需要竖屏
        return UIInterfaceOrientationMaskPortrait;
    }
}

#pragma mark - UISceneSession lifecycle

/*!
 Xcode 11 创建新项目默认有两个 delegate 文件: AppDelegate, SceneDelegate,  info.plist 文件中多一个 Application Scene Manifest
 适配方案1: iPadOS多窗口APP, 如果不需要可以删除:
    1.删除掉info.plist中Application Scene Manifest选项，同时，文件SceneDelegate可删除可不删
    2.注释掉以下两个方法, 否则黑屏(13.0以上黑屏 , 13.0以下未测试)
 适配方案2: 添加版本判断，利用@available
 详情参考:  https://blog.csdn.net/weixin_38735568/article/details/101266408
 */
//- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
//    // Called when a new scene session is being created.
//    // Use this method to select a configuration to create the new scene with.
//    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
//}
//
//- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
//    // Called when the user discards a scene session.
//    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//}


@end
