//
//  AddUserCell.m
//  Riese
//
//  Created by air on 2019/10/22.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "AddUserCell.h"

@interface AddUserCell ()

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *addLabel;

@end

@implementation AddUserCell

- (void)layoutSubviews {
    self.bgView.backgroundColor = UIColorFromHex(0x4FA1A5);
    self.addLabel.text = NSLocalizedString(@"Add User", nil);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
