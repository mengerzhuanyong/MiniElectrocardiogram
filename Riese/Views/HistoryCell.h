//
//  HistoryCell.h
//  Riese
//
//  Created by air on 2019/10/26.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HistoryCell : UITableViewCell

@property (nonatomic, strong) NSDictionary *dataDic;
//@property (nonatomic, strong) UILongPressGestureRecognizer * longPressGesture;
@property (weak, nonatomic) IBOutlet UILongPressGestureRecognizer *longPressGesture;

@end

NS_ASSUME_NONNULL_END
