//
//  ContactCell.h
//  Riese
//
//  Created by air on 2019/10/26.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContactCell : UITableViewCell

@property (nonatomic, strong) NSString *titleStr;

@end

NS_ASSUME_NONNULL_END
