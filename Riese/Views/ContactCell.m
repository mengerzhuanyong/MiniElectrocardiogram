//
//  ContactCell.m
//  Riese
//
//  Created by air on 2019/10/26.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "ContactCell.h"

@interface ContactCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ContactCell

- (void)layoutSubviews {
    self.titleLabel.text = self.titleStr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
