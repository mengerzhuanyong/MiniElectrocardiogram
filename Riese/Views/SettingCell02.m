//
//  SettingCell02.m
//  Riese
//
//  Created by air on 2019/10/26.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "SettingCell02.h"

@interface SettingCell02 ()

@property (weak, nonatomic) IBOutlet UIButton *clearBtn;
@property (weak, nonatomic) IBOutlet UILabel *clearLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldLabel;

@end

@implementation SettingCell02

- (void)layoutSubviews {
    self.clearBtn.userInteractionEnabled = NO;
    self.clearLabel.text = NSLocalizedString(@"Clear Memory", nil);
    self.oldLabel.text = NSLocalizedString(@"Old recordings in your device", nil);
    self.clearBtn.backgroundColor =UIColorFromHex(0x5CBBC0);//[UIColor whiteColor];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
