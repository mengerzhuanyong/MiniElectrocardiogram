//
//  KnowledgeCell.m
//  Riese
//
//  Created by air on 2019/11/4.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "KnowledgeCell.h"

@interface KnowledgeCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end

@implementation KnowledgeCell

- (void)layoutSubviews {
    self.titleLabel.text = self.titleStr;
    self.detailLabel.layer.cornerRadius = 13;
    self.detailLabel.clipsToBounds = YES;
    self.detailLabel.textColor = UIColorFromHex(0x5CBBC0);//[UIColor whiteColor];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
