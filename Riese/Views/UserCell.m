//
//  UserCell.m
//  Riese
//
//  Created by air on 2019/10/22.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "UserCell.h"

@interface UserCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIView *xianV;

@end

@implementation UserCell

- (void)layoutSubviews {
    if ([self.dataDic[@"avatar"] length] > 0) {
        NSData *data = defaults(self.dataDic[@"avatar"]);
        self.avatar.image = [UIImage imageWithData:data];
    }
    else {
        self.avatar.image = [UIImage imageNamed:@"defaultUser"];
    }
    self.nameLabel.text = self.dataDic[@"nickname"];
//    headerView.backgroundColor = UIColorFromHex(0x5CBBC0);//[UIColor colorWithRed:245 / 255.0 green:247 / 255.0 blue:250 / 255.0 alpha:1.0];

    
    self.backgroundColor = UIColorFromHex(0x379297);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
