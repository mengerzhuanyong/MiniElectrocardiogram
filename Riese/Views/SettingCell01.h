//
//  SettingCell01.h
//  Riese
//
//  Created by air on 2019/10/26.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingCell01 : UITableViewCell

@property (weak, nonatomic) IBOutlet UISwitch *soundSwitch;

@end

NS_ASSUME_NONNULL_END
