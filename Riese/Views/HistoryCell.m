//
//  HistoryCell.m
//  Riese
//
//  Created by air on 2019/10/26.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "HistoryCell.h"

@interface HistoryCell ()

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIView *blueView;
@property (weak, nonatomic) IBOutlet UILabel *timeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *hrTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hrValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

@end

@implementation HistoryCell

- (void)layoutSubviews {
    self.bgView.layer.cornerRadius = 4;
    self.bgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.2].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 5;
    
    self.timeTitleLabel.text = [NSString stringWithFormat:@"%@ : ", NSLocalizedString(@"Time", nil)];
    
    NSString *year = self.dataDic[@"year"];
    NSString *month = [Common turnString:self.dataDic[@"month"]];
    NSString *day = [Common turnString:self.dataDic[@"day"]];
    NSString *hour = [Common turnString:self.dataDic[@"hour"]];
    NSString *min = [Common turnString:self.dataDic[@"min"]];
    NSString *sec = [Common turnString:self.dataDic[@"sec"]];
    
    self.timeValueLabel.text = [NSString stringWithFormat:@"%@/%@/%@  %@:%@:%@", month, day, year, hour, min, sec];
    
    self.hrTitleLabel.text = [NSString stringWithFormat:@"%@ : ", NSLocalizedString(@"HR", nil)];
    self.hrValueLabel.text = [NSString stringWithFormat:@"%@bpm", self.dataDic[@"HeartRate"]];
    
    NSInteger duration = [self.dataDic[@"TimeLen"] integerValue]/250;
    self.durationLabel.text = [NSString stringWithFormat:@"%@ : %lds", NSLocalizedString(@"Duration", nil), duration];
    
    self.resultTitleLabel.text = [NSString stringWithFormat:@"%@ : ", NSLocalizedString(@"Result", nil)];
    
    // 结果
    NSString *result = @"";
    NSArray *resultArr = @[
        self.dataDic[@"sort1"],
        self.dataDic[@"sort2"],
        self.dataDic[@"sort3"],
        self.dataDic[@"sort4"],
        self.dataDic[@"sort5"],
        self.dataDic[@"sort6"],
    ];
    NSArray *resultStrArr = @[
        NSLocalizedString(@"noabnormal", nil),
        NSLocalizedString(@"missedbeat", nil),
        NSLocalizedString(@"accidentalVPB", nil),
        NSLocalizedString(@"trigeminy", nil),
        NSLocalizedString(@"VPBbigeminy", nil),
        NSLocalizedString(@"VPBcouple", nil),
        NSLocalizedString(@"VPBrunsof3", nil),
        NSLocalizedString(@"VPBrunsof4", nil),
        NSLocalizedString(@"ront", nil),
        NSLocalizedString(@"bradycardia", nil),
        NSLocalizedString(@"tachycardia", nil),
        NSLocalizedString(@"arrhythmia", nil),
        NSLocalizedString(@"STelevation", nil),
        NSLocalizedString(@"STdepression", nil)
    ];
    
    for (int i = 0; i < resultArr.count; i++) {
        NSString *str = [resultArr[i] integerValue] == 0 ? @"" : resultStrArr[[resultArr[i] integerValue]];
        if (str.length > 0) {
            result = [NSString stringWithFormat:@"%@ %@", result, str];
        }
    }
    
    if (result.length == 0) {
        result = resultStrArr[0];
    }
    
    self.resultValueLabel.text = result;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
