//
//  UserCell.h
//  Riese
//
//  Created by air on 2019/10/22.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *selectImgV;
@property (nonatomic, strong) NSDictionary *dataDic;
@property (weak, nonatomic) IBOutlet UIButton *editUserBtn;


@end

NS_ASSUME_NONNULL_END
