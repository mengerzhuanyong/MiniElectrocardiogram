//
//  HomeViewCell.m
//  Riese
//
//  Created by air on 2019/10/18.
//  Copyright © 2019 wangjinyu. All rights reserved.
//
#define fontOfText 14
#define color_text UIColorFromHex(0x666666)
#define kCombileABStr(a,b) (NSString *)[NSString stringWithFormat:@"%@%@",a,b]
#import "HomeViewCell.h"
@interface HomeViewCell()

@property (nonatomic, strong) UIView *v_bgV;
@property (nonatomic, strong) UIImageView *imgV_clock;
@property (nonatomic, strong) UILabel *lab_time1;

@property (nonatomic, strong) UIImageView *imgV_heart;
@property (nonatomic, strong) UILabel * lab_HR;

@property (nonatomic, strong) UIImageView * imgV_dur;
@property (nonatomic, strong) UILabel * lab_dur;

@property (nonatomic, strong) UIImageView * imgV_rem;
@property (nonatomic, strong) UILabel * lab_rem;


@end
@implementation HomeViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.v_bgV = [UIView new];
        
        self.imgV_clock = [UIImageView new];
        self.lab_time1 = [UILabel new];
        
        self.imgV_heart = [UIImageView new];
        self.lab_HR = [UILabel new];
        
        self.imgV_dur = [UIImageView new];
        self.lab_dur = [UILabel new];
        
        self.imgV_rem = [UIImageView new];
        self.lab_rem = [UILabel new];
        self.longPressGesture = [UILongPressGestureRecognizer new];
        
        [self.contentView addSubview:self.v_bgV];
        [self.v_bgV addSubview:self.imgV_clock];
        [self.v_bgV addSubview:self.lab_time1];
        
        [self.v_bgV addSubview:self.imgV_heart];
        [self.v_bgV addSubview:self.lab_HR];
        
        [self.v_bgV addSubview:self.imgV_dur];
        [self.v_bgV addSubview:self.lab_dur];
        
        [self.v_bgV addSubview:self.imgV_rem];
        [self.v_bgV addSubview:self.lab_rem];
        [self.contentView addGestureRecognizer:self.longPressGesture];
    }

    return self;
}

- (void)layoutSubviews {
    self.contentView.backgroundColor = [UIColor clearColor];
    self.v_bgV.sd_layout.leftSpaceToView(self.contentView, 15).topSpaceToView(self.contentView, 10).widthIs(self.contentView.width - 30).heightIs(self.contentView.height - 10);
    self.v_bgV.layer.cornerRadius = 10;
    self.v_bgV.backgroundColor = [UIColor whiteColor];
    self.v_bgV.clipsToBounds = YES;
    
    self.imgV_clock.sd_layout.leftSpaceToView(self.v_bgV, 12).topSpaceToView(self.v_bgV, 18).widthIs(15).heightIs(15);
    self.imgV_clock.image = [UIImage imageNamed:@"shijian_3"];
    
    self.lab_time1.sd_layout.leftSpaceToView(self.imgV_clock, 5).centerYEqualToView(self.imgV_clock).rightSpaceToView(self.v_bgV, 12).heightIs(20);
    
    NSString *year = self.dataDic[@"year"];
    NSString *month = [Common turnString:self.dataDic[@"month"]];
    NSString *day = [Common turnString:self.dataDic[@"day"]];
    NSString *hour = [Common turnString:self.dataDic[@"hour"]];
    NSString *min = [Common turnString:self.dataDic[@"min"]];
    NSString *sec = [Common turnString:self.dataDic[@"sec"]];
    
    self.lab_time1.text = [NSString stringWithFormat:@"%@: %@/%@/%@  %@:%@:%@",NSLocalizedString(@"Time", nil), year, month, day, hour, min, sec];
    self.lab_time1.font = [UIFont systemFontOfSize:fontOfText];
    self.lab_time1.textColor = color_text;

    self.imgV_heart.sd_layout.leftEqualToView(self.imgV_clock).topSpaceToView(self.imgV_clock, 20).widthIs(14).heightIs(13);
    self.imgV_heart.image = [UIImage imageNamed:@"home_c_zan"];

    self.lab_HR.sd_layout.leftSpaceToView(self.imgV_heart, 5).centerYEqualToView(self.imgV_heart).heightIs(20);
    [self.lab_HR setSingleLineAutoResizeWithMaxWidth:0];
    self.lab_HR.text = [NSString stringWithFormat:@"%@: %@bpm", NSLocalizedString(@"HR", nil),  self.dataDic[@"HeartRate"]];
    self.lab_HR.font = [UIFont systemFontOfSize:fontOfText];
    self.lab_HR.textColor = color_text;
    
    self.imgV_dur.sd_layout.leftSpaceToView(self.v_bgV, self.v_bgV.width / 2).centerYEqualToView(self.imgV_heart).widthIs(17).heightIs(8);
    self.imgV_dur.image = [UIImage imageNamed:@"home_c_dur"];
    
    self.lab_dur.sd_layout.leftSpaceToView(self.imgV_dur, 5).centerYEqualToView(self.imgV_dur).rightSpaceToView(self.v_bgV, 12).heightIs(20);
    self.lab_dur.textColor = color_text;
    self.lab_dur.font = [UIFont systemFontOfSize:fontOfText];
    NSInteger duration = [self.dataDic[@"TimeLen"] integerValue]/250;
    self.lab_dur.text = [NSString stringWithFormat:@"%@: %lds", NSLocalizedString(@"Duration", nil), duration];
    
    self.imgV_rem.sd_layout.leftEqualToView(self.imgV_heart).topSpaceToView(self.imgV_heart, 20).widthIs(14).heightIs(11);
    self.imgV_rem.image = [UIImage imageNamed:@"home_c_rem"];
    
    // 结果
    NSString *result = @"";
    NSArray *resultArr = @[
        self.dataDic[@"sort1"],
        self.dataDic[@"sort2"],
        self.dataDic[@"sort3"],
        self.dataDic[@"sort4"],
        self.dataDic[@"sort5"],
        self.dataDic[@"sort6"],
    ];
    NSArray *resultStrArr = @[
        NSLocalizedString(@"noabnormal", nil),
        NSLocalizedString(@"missedbeat", nil),
        NSLocalizedString(@"accidentalVPB", nil),
        NSLocalizedString(@"trigeminy", nil),
        NSLocalizedString(@"VPBbigeminy", nil),
        NSLocalizedString(@"VPBcouple", nil),
        NSLocalizedString(@"VPBrunsof3", nil),
        NSLocalizedString(@"VPBrunsof4", nil),
        NSLocalizedString(@"ront", nil),
        NSLocalizedString(@"bradycardia", nil),
        NSLocalizedString(@"tachycardia", nil),
        NSLocalizedString(@"arrhythmia", nil),
        NSLocalizedString(@"STelevation", nil),
        NSLocalizedString(@"STdepression", nil)
    ];
    
    for (int i = 0; i < resultArr.count; i++) {
        NSString *str = [resultArr[i] integerValue] == 0 ? @"" : resultStrArr[[resultArr[i] integerValue]];
        if (str.length > 0) {
            result = [NSString stringWithFormat:@"%@ %@", result, str];
        }
    }
    
    if (result.length == 0) {
        result = resultStrArr[0];
    }
    
    self.lab_rem.sd_layout.leftSpaceToView(self.imgV_rem, 5).centerYEqualToView(self.imgV_rem).rightSpaceToView(self.v_bgV, 12).heightIs(20);
    self.lab_rem.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Result", nil), result];
    self.lab_rem.textColor = color_text;
    self.lab_rem.font = [UIFont systemFontOfSize:fontOfText];
    [self.lab_rem sizeToFit];
    
    self.longPressGesture.minimumPressDuration = 1.0f;//设置长按 时间
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
