//
//  KnowledgeCell.h
//  Riese
//
//  Created by air on 2019/11/4.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KnowledgeCell : UITableViewCell

@property (nonatomic, strong) NSString *titleStr;

@end

NS_ASSUME_NONNULL_END
