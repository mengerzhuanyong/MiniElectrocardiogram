//
//  ConnectCell.m
//  Riese
//
//  Created by air on 2019/10/23.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "ConnectCell.h"

@interface ConnectCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ConnectCell

- (void)layoutSubviews {
    self.titleLabel.text = self.titleStr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
