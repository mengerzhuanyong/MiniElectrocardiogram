//
//  SettingCell01.m
//  Riese
//
//  Created by air on 2019/10/26.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import "SettingCell01.h"

@interface SettingCell01 ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation SettingCell01

- (void)layoutSubviews {
    self.soundSwitch.transform = CGAffineTransformMakeScale(1.18, 1);
    self.titleLabel.text = NSLocalizedString(@"Heartbeat Sound", nil);
    if (defaults(@"DeviceSound")) {
        if ([defaults(@"DeviceSound") isEqualToString:@"0"]) {
            [self.soundSwitch setOn:YES];
        }
        else {
            [self.soundSwitch setOn:NO];
        }
    }
    else {
        [self.soundSwitch setOn:NO];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
