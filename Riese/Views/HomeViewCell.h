//
//  HomeViewCell.h
//  Riese
//
//  Created by air on 2019/10/18.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeViewCell : UITableViewCell

@property (nonatomic, strong) NSDictionary *dataDic;
@property (nonatomic, strong) UILongPressGestureRecognizer * longPressGesture;

@end

NS_ASSUME_NONNULL_END
