//
//  SceneDelegate.h
//  Riese
//
//  Created by air on 2019/10/17.
//  Copyright © 2019 wangjinyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

