//
//  GBPopMenuView.m
//  WJMenu
//
//  Created by mac on 2016/12/2.
//  Copyright © 2016年 wujunyang. All rights reserved.
//

#import "GBPopMenuView.h"
#import "GBIndicatorView.h"
#define GBScreen_Height      [[UIScreen mainScreen] bounds].size.height
#define GBScreen_Width       [[UIScreen mainScreen] bounds].size.width

#define paddxSide (SCREEN_WIDTH *(15./375))

@interface GBPopMenuView()
{
    UIView *_backgroundView;
    UIButton *_select_button;
}
@end


@implementation GBPopMenuView


-(id)initWithTitles
{
    if (self == [super init]) {
        _titles = [NSMutableArray array];
    }
    return self;
}
-(void)show:(UIView*)contentView Poprect:(CGRect)rect;
{
    _isShow = YES;
    NSInteger columnCount = 4;
    NSInteger itemWidth = (GBScreen_Width - SCREEN_WIDTH*(15./375)*5)/columnCount;
    
    UIButton *_title_button;
    _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(rect), GBScreen_Width, GBScreen_Height)];
    _backgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    [contentView.superview addSubview:_backgroundView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimiss_background:)];
    [_backgroundView addGestureRecognizer:tap];
    

    self.frame = CGRectMake(0,0, GBScreen_Width, 0);
    self.backgroundColor = kColor(247, 247, 249);

    self.layer.masksToBounds = YES;
    [_backgroundView addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame=self.frame;
        frame.size.height=200;
        self.frame=frame;
    }];
    
    
    if (_titles.count) {
        for (int index = 0; index < _titles.count; index++) {
            NSUInteger columnIndex =  index % columnCount;//第几列
            NSUInteger rowIndex = index / columnCount;//第几行
            UIButton *title_button = [[UIButton alloc] initWithFrame:CGRectMake(columnIndex?(itemWidth+paddxSide)*columnIndex+paddxSide:paddxSide, rowIndex?(SCREEN_HEIGHT*(29./667)+paddxSide)*rowIndex+paddxSide:paddxSide, itemWidth, SCREEN_HEIGHT*(29./667))];
            title_button.tag = index;
            [title_button setTitle:_titles[index] forState:UIControlStateNormal];
            [title_button setTitleColor:(self.selectIndex == index)?[UIColor colorWithRed:248./255 green:0 blue:0 alpha:1]:[UIColor colorWithRed:152./255 green:155./255 blue:176./255 alpha:1] forState:UIControlStateNormal];
            [title_button setBackgroundColor:(self.selectIndex == index)?[UIColor colorWithRed:248./255 green:0 blue:0 alpha:0.05]:[UIColor clearColor]];
            title_button.layer.borderColor = (self.selectIndex == index)?[UIColor colorWithRed:248./255 green:0 blue:0 alpha:1].CGColor:[UIColor colorWithRed:185./255 green:188./255 blue:211./255 alpha:1].CGColor;
            if (self.selectIndex == index) {
                _select_button = title_button;
            }
            title_button.titleLabel.font =[UIFont fontWithName:@"PingFangSC-Medium" size:14];
            title_button.layer.borderWidth = 0.5f;
            title_button.layer.cornerRadius = SCREEN_HEIGHT*(29./667)/2;
            title_button.layer.masksToBounds = YES;
            [title_button addTarget:self action:@selector(select_button:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:title_button];
            _title_button = title_button;
            
        }
        self.frame =
        CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, CGRectGetMaxY(_title_button.frame)+16);
        
    }
}


-(void)select_button:(UIButton*)sender
{
    if (self.selectIndex != sender.tag) {
        [_select_button setTitleColor:self.noSlectedColor forState:UIControlStateNormal];
        _select_button.layer.borderColor = self.noSlectedColor.CGColor;
        [sender setTitleColor:self.selectedColor  forState:UIControlStateNormal];
        sender.layer.borderColor = self.selectedColor.CGColor;
        if (self.didSelectItemIndex) {
            self.didSelectItemIndex(sender.tag);
        }
    }
    _select_button = sender;
}


-(void)dimiss_background:(UITapGestureRecognizer*)tap
{
    [self dismiss];
    if (self.dismissPopView) {
        self.dismissPopView();
    }
}


-(void)dismiss
{
    _isShow = NO;
    [_backgroundView removeFromSuperview];
    _backgroundView = nil;
    [self removeFromSuperview];
}

@end
