//
//  HeartLive.h
//  HeartRateCurve
//
//  Created by IOS－001 on 14-4-23.
//  Copyright (c) 2014年 N/A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PointContainer : NSObject

@property (nonatomic , readonly) NSInteger numberOfRefreshElements;
@property (nonatomic , readonly) NSInteger numberOfTranslationElements;
@property (nonatomic , readonly) CGPoint *refreshPointContainer;
@property (nonatomic , readonly) CGPoint *translationPointContainer;

+ (PointContainer *)sharedContainer;
/*!**销毁单例***/
+ (PointContainer *)destroyInstance;

//刷新变换
- (void)addPointAsRefreshChangeform:(CGPoint)point;
//平移变换
- (void)addPointAsTranslationChangeform:(CGPoint)point;

- (void)fixOffSet;

- (void)clear;

@end



@interface HeartLive : UIView

//static const NSInteger kMaxContainerCapacity = 2500;
- (void)fireDrawingWithPoints:(CGPoint *)points pointsCount:(NSInteger)count;

@end

