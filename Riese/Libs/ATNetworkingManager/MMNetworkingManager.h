//
//  MMNetworkingManager.h
//  MessageManagement
//
//  Created by 奚春竹 on 17/3/24.
//  Copyright © 2017年 奚春竹. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SuccessBlock)(NSDictionary *object);
typedef void (^FailureBlock)(NSError *error);
@interface MMNetworkingManager : NSObject

@property (nonatomic, copy) SuccessBlock successBlock;
@property (nonatomic, copy) FailureBlock failureBlock;

/**
 GET请求

 @param urlString url
 @param parameters 参数
 @param successBlock 成功的回调
 @param failureBlock 失败的回调
 */
+ (void)GET:(NSString *)urlString
     params:(id )parameters
    success:(SuccessBlock)successBlock
    failure:(FailureBlock)failureBlock;


/**
 POST请求
 
 @param urlString url
 @param params 参数
 @param successBlock 成功的回调
 @param failureBlock 失败的回调
 */
+ (void)POST:(NSString *)urlString
      params:(id )params
     success:(SuccessBlock)successBlock
     failure:(FailureBlock)failureBlock;

/**
 POST json请求
 
 @param urlString url
 @param json json数据
 @param successBlock 成功的回调
 @param failureBlock 失败的回调
 */
+ (void)POST:(NSString *)urlString
        json:(id )json
     success:(SuccessBlock)successBlock
     failure:(FailureBlock)failureBlock;

/**
 DELETE请求

 @param urlString url
 @param params 参数
 @param successBlock 成功的回调
 @param failureBlock 失败的回调
 */
+ (void)DELETE:(NSString *)urlString
        params:(id )params
       success:(SuccessBlock)successBlock
       failure:(FailureBlock)failureBlock;


+(void)POSTImage:(NSString *)urlString
          params:(NSDictionary *)params
      imagesName:(NSString *)name
      imageArray:(NSArray *)imageArray
         success:(SuccessBlock)successBlock
         failure:(FailureBlock)failureBlock;


+(void)POSTListAndImage:(NSString *)urlString
                 params:(NSDictionary *)params
             imagesName:(NSString *)name
             imageArray:(NSArray *)imageArray
                success:(SuccessBlock)successBlock
                failure:(FailureBlock)failureBlock;
@end
